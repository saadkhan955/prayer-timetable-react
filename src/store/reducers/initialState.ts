// @ts-ignore: Unreachable code error
import moment from 'moment-hijri'

import * as timetable from '../../config/timetable.json'
import * as settings from '../../config/settings.json'
import { prayersCalc } from 'prayer-timetable-lib'

moment.locale('en') // to avoid indian numbers

const prayers = prayersCalc(
  timetable,
  settings,
  settings.jamaahShow,
  'Europe/Dublin',
)

// console.log("prayers!@", prayers)

export interface State {
  timetable: object
  settings: {
    title: string
    description: string
    city: string
    lang: string
    online: boolean
    logo: string
    log: boolean
    updateInterval: number
    template: string
    url: {
        settings: string
        timetable: string,
    }
    announcement: string
    text: {
        en: string
        ar: string,
    }
    hijrioffset: string
    join: string
    jummuahtime: number[]
    taraweehtime: number[]
    jamaahShow: boolean
    jamaahmethods: string[]
    jamaahoffsets: number[][]
    labels: {
        jummuah: string
        taraweeh: string
        prayer: string
        adhan: string
        iqamah: string
        prepare: string
        ramadancountdown: string
        names: string[],
    }
    switch: boolean
    updated: number,
  }
  prayers: {
    countDown: {
      duration: any
      name: string
      time: any,
    }
    countUp: {
      duration: any
      name: string
      time: any,
    }
    current: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    focus: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    hijri: any
    isAfterIsha: boolean
    isJamaahPending: boolean
    next: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    }
    now: any
    percentage: number
    prayers: {
      today: object
      tomorrow: object
      yesterday: object,
    }
    previous: {
      dstAdjust: number
      hasPassed: boolean
      index: number
      isJamaahPending: boolean
      jtime: any
      name: string
      time: any
      when: string,
    },
  }
  day: {
    now: any
    hijri: any
    overlayActive: boolean
    overlayTitle: string,
  }
  update: {
    downloaded: any,
    refreshed: any,
    success: boolean,
  },
}

export const initialState: State = {
  timetable,
  settings,
  prayers,
  // prayers: {
  //   prayers: {
  //     today: [
  //       {
  //         isJamaahPending: false,
  //         index: 0,
  //         hasPassed: true,
  //         name: 'fajr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 1,
  //         hasPassed: true,
  //         name: 'shurooq',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 2,
  //         hasPassed: true,
  //         name: 'dhuhr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 3,
  //         hasPassed: true,
  //         name: 'asr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 4,
  //         hasPassed: true,
  //         name: 'maghrib',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 5,
  //         hasPassed: true,
  //         name: 'isha',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //     ],
  //     tomorrow: [
  //       {
  //         isJamaahPending: false,
  //         index: 0,
  //         hasPassed: true,
  //         name: 'fajr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 1,
  //         hasPassed: true,
  //         name: 'shurooq',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 2,
  //         hasPassed: true,
  //         name: 'dhuhr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 3,
  //         hasPassed: true,
  //         name: 'asr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 4,
  //         hasPassed: true,
  //         name: 'maghrib',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 5,
  //         hasPassed: true,
  //         name: 'isha',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //     ],
  //     yesterday: [
  //       {
  //         isJamaahPending: false,
  //         index: 0,
  //         hasPassed: true,
  //         name: 'fajr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 1,
  //         hasPassed: true,
  //         name: 'shurooq',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 2,
  //         hasPassed: true,
  //         name: 'dhuhr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 3,
  //         hasPassed: true,
  //         name: 'asr',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 4,
  //         hasPassed: true,
  //         name: 'maghrib',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //       {
  //         isJamaahPending: false,
  //         index: 5,
  //         hasPassed: true,
  //         name: 'isha',
  //         when: 'today',
  //         time: { format: () => {} },
  //         jtime: { format: () => {} },
  //       },
  //     ],
  //   },
  //   percentage: 0,
  //   countUp: {
  //     name: '',
  //     time: { format: () => {} },
  //     duration: { hours: () => '--', minutes: () => '--', seconds: () => '--' },
  //   },
  //   countDown: {
  //     name: '',
  //     time: { format: () => {} },
  //     duration: { hours: () => '--', minutes: () => '--', seconds: () => '--' },
  //   },
  //   isAfterIsha: false,
  //   next: {},
  //   focus: {},
  // },
  day: {
    // ramadanCountdown: '',
    now: moment(),
    hijri: moment(),
    overlayActive: false,
    overlayTitle: '...',
  },
  update: {
    downloaded: null,
    refreshed: null,
    success: false,
  },
}
