import { GET_PRAYERS } from '../constants/actionTypes'
import { initialState } from './initialState'

const prayersReducer = (state = initialState.prayers, action: any) => {
  switch (action.type) {
    case GET_PRAYERS:
      // console.log('toggled in prayers reducer', action)
      return {
        ...state,
        prayers: action.prayers,
        percentage: action.percentage,
        countUp: action.countUp,
        countDown: action.countDown,
        next: action.next,
        isAfterIsha: action.isAfterIsha,
        focus: action.focus,
      }

    default:
      return state
  }
}

export default prayersReducer
