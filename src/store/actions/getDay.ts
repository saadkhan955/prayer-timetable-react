import { dayCalc } from 'prayer-timetable-lib'
// dayCalc = (offsetDay = 0, offSetHour = 0, hijrioffset = 0, city = 'Europe/Dublin')
// return { now, month, date, start, end, hijri, dstAdjust }
// @ts-ignore
import moment from 'moment-hijri'
import store from '../index'
import { GET_DAY } from '../constants/actionTypes'

moment.locale('en') // to avoid indian numbers

// console.log('STORE', store.getState())

const getDay = () => {
  const { now, hijri } = dayCalc(0, 0, {
    hijrioffset: store.getState().settings.hijrioffset,
  })

  // OVERLAY
  // const jummuahTime = moment({
  //   hour: 19,
  //   minute: 0,
  // }).day(6) // test
  const jummuahTime = moment({
    hour: store.getState().settings.jummuahtime[0],
    minute: store.getState().settings.jummuahtime[1],
  }).day(5)
  const taraweehTime = moment({
    hour: store.getState().settings.taraweehtime[0],
    minute: store.getState().settings.taraweehtime[1],
  }) // .iMonth(8),

  let overlayActive
  let overlayTitle

  if (moment().isBetween(jummuahTime, jummuahTime.clone().add(1, 'hour'))) {
    overlayActive = true
    overlayTitle = 'Jummuah Prayer'
  } else if (
    // taraweeh in ramadan first part
    moment().format('iM') === '9' &&
    moment().isBetween(taraweehTime, taraweehTime.clone().add(2, 'hour'))
  ) {
    overlayActive = true
    overlayTitle = 'Taraweeh Prayer'
  } else if (
    // taraweeh in ramadan second part
    moment().format('iM') === '9' &&
    moment().isBetween(
      moment().startOf('day'),
      moment()
        .startOf('day')
        .clone()
        .add(1, 'hour'),
    )
  ) {
    overlayActive = true
    overlayTitle = 'Taraweeh Prayer'
  } else {
    overlayActive = false
    overlayTitle = ' ... '
  }

  return store.dispatch({
    type: GET_DAY,
    now,
    hijri,
    overlayActive,
    overlayTitle,
    // prayers: store.getState().prayers,
  })
}

export default getDay
