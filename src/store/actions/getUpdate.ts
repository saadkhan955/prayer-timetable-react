// @ts-ignore
import moment from 'moment-hijri'
import axios from 'axios'
import localforage from 'localforage'

import checkIfNeeded from './checkIfNeeded'
import * as defsettings from '../../config/settings.json'

import store from '../index'

import { GET_UPDATE, GET_LOCAL } from '../constants/actionTypes'
// import { update as runUpdate } from './index'

const API_URI = defsettings.url.settings

moment.locale('en') // to avoid indian numbers

const { log } = store.getState().settings

/**
 * **********
 * ONLINE
 * **********
 */

const online = async (force: boolean) => {
  const { settings } = store.getState()
  const { updateInterval } = settings
  const { update } = store.getState()
  const { downloaded, success } = update

  // moment.isMoment(downloadFailTime) && console.log(downloadFailTime.format('HH:mm:ss'))

  const needed = checkIfNeeded(downloaded, success, updateInterval) || force

  let newSettings
  let newDownloaded
  let newSuccess

  if (needed) {
    try {
      const { data: newsettings } = await axios.get(API_URI, {
        method: 'GET',
        // mode: 'cors',
        // crossdomain: true,
        // withCredentials: true,
      })

      newSettings = newsettings
      newDownloaded = moment()
      newSuccess = true

      log && console.log('downloaded:', moment().format('HH:mm:ss'))
    } catch (error) {
      log && console.log('### SET ###', error)

      newSettings = settings
      newDownloaded = downloaded
      newSuccess = false
      log && console.log('error fetching update', error)
    }
  } else {
    // in case not needed
    newSettings = settings
    newDownloaded = downloaded
    newSuccess = true
    log && console.log('update not needed')
  }

  const result = {
    settings: newSettings,
    downloaded: newDownloaded,
    success: newSuccess,
  }
  await localforage.setItem('settings', { ...settings, ...newSettings })
  await localforage.setItem('downloaded', newDownloaded.valueOf())
  // log && console.log('returned new:', result)
  return result
}

/**
 * **********
 * LOCAL
 * **********
 */
const local = async () => {
  let settings
  let downloaded
  try {
    settings = await localforage.getItem('settings')
    downloaded = moment(await localforage.getItem('downloaded'))
    log && console.log('got storredsettings', await settings)
    log && console.log('got storredupdate', await downloaded)
  } catch (error) {
    /* eslint-disable prefer-destructuring */
    settings = store.getState().settings
    downloaded = store.getState().update.downloaded
    log && console.log('failed geting local storage', error)
  }
  // console.log('newsettings:', newsettings)

  /* eslint-disable babel/no-unused-expressions */
  // logging && console.log('got api settings', newsettings)

  const result = {
    settings,
    downloaded,
  }
  return result
}

/**
 * **********
 * EXPORTS
 * **********
 */
const getLocal = async (force = false) => {
  try {
    store.dispatch({ type: GET_LOCAL, payload: await local() })
  } catch (error) {
    log && console.log('error - local update', error)
  }
}

const getUpdate = async (force = false) => {
  if (defsettings.online) {
    try {
      const payload = await online(force)
      store.dispatch({ type: GET_UPDATE, payload })
      log && console.log('online update checked')
    } catch (error) {
      log && console.log('error - online update', error)
    }
  }
}

export default getUpdate
export { getUpdate, getLocal }
