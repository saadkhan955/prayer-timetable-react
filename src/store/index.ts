// import { createStore } from 'redux'
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './reducers'

// const store = createStore(rootReducer)

// console.log(store)
import { helloSaga } from './sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
sagaMiddleware.run(helloSaga)

// const action = (type) => store.dispatch({ type })

export default store
