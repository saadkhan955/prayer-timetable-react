/* eslint-env browser */

import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store/index'
import App from './App'

// actions
import { tick, update } from './store/actions/setIntervals'
import { getLocal } from './store/actions/getUpdate'

// EXPOSE store.getState() and others to browser console
import './store/window'
// remove service worker
// import './helpers/removeServiceWorker'

const start = async () => {
  await getLocal()
  await tick()
  await update()
}
start()

// PWA Stuff
// ;(function() {
//   if ('serviceWorker' in navigator) {
//     navigator.serviceWorker
//       .register('./service-worker.js', { scope: '/' })
//       .then(() => console.log('Service Worker registered successfully.'))
//       .catch((error) => console.log('Service Worker registration failed:', error))
//   }
// })()

// render(<Index />, document.getElementById('index'))
render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
