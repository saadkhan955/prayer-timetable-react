import * as React from 'react'

import { connect } from 'react-redux'

import styles from '../styles'
// @ts-ignore
import logo from '../config/logo.svg'

import { State } from '../store/reducers/initialState'
type StateProps = Pick<State, 'day'>

const HeaderStyle = styles.header

const logoImg = <img src={logo} alt="logo" />

const mapStateToProps = (state: StateProps) => ({ day: state.day })

const ConnectedHeader = ({ day }: StateProps) => (
  <HeaderStyle>
    <div>{day.now.format('ddd DD MMMM YYYY')}</div>
    <div className="logo">{logoImg}</div>
    <div>{day.hijri.format('iDD iMMMM iYYYY')}</div>
    {/* <div className="center">{settings.title}</div> */}
  </HeaderStyle>
)
const Header = connect(mapStateToProps)(ConnectedHeader)
export default Header
