import * as React from 'react'
import { connect } from 'react-redux'

import styles from '../styles'
import { State } from '../store/reducers/initialState'

const PrayersStyle = styles.prayers
const PrayerStyle = styles.prayer

type StateProps = Pick<State, 'settings' | 'prayers'>

const mapStateToProps = (state: StateProps) => ({ prayers: state.prayers, settings: state.settings })

const renderPrayers = (settings: any, prayers: any) => {
  const { next, isAfterIsha, focus } = prayers
  const { join, jamaahShow } = settings

  const prayerList = isAfterIsha ? prayers.prayers.tomorrow : prayers.prayers.today

  const isNext = (prayer: any) => (jamaahShow ? !!(focus.name === prayer.name) : !!(next.name === prayer.name))

  const adhan = (prayer: any) => {
    let result
    if (jamaahShow && (join === '1' || join === true) && prayer.name === 'isha') {
      result = <div className="adhanTime">after</div>
    } else {
      result = <div className="adhanTime right">{prayer.time.format('H:mm')}</div>
    }
    return result
  }

  const iqamah = (prayer: any) => {
    let result
    if (jamaahShow && (join === '1' || join === true) && prayer.name === 'isha') {
      result = <div className="iqamahTime">maghrib</div>
    } else if (jamaahShow && prayer.name !== 'shurooq') {
      result = (
        <div className={`iqamahTime${prayer.isJamaahPending ? ' pending' : ''}`}>{prayer.jtime.format('H:mm')}</div>
      )
    } else if (jamaahShow && prayer.name === 'shurooq') {
      result = <div className="iqamahTime" />
    } else {
      result = ''
    }
    return result
  }

  /* eslint-disable react/no-array-index-key */
  return prayerList.map((prayer: any, index: any) => (
    <PrayerStyle key={index} prayer={prayer} next={isNext(prayer)}>
      {/* {console.log(prayers.next.name, isNext(prayer))} */}
      <div className="prayerName">{prayer.name}</div>
      {adhan(prayer)}
      {iqamah(prayer)}
    </PrayerStyle>
  ))
}

const ConnectedPrayers = ({ settings, prayers }: StateProps) => (
  <PrayersStyle>
    <div className="prayerHeader">
      <div className="prayerName">Prayer</div>
      <div>Adhan</div>
      {settings.jamaahShow ? <div>Iqamah</div> : null}
    </div>
    {renderPrayers(settings, prayers)}
  </PrayersStyle>
)
const Prayers = connect(mapStateToProps)(ConnectedPrayers)
export default Prayers
