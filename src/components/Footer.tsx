import * as React from 'react'

import { connect } from 'react-redux'

// @ts-ignore
import moment from 'moment-hijri'

// @ts-ignore
import { Offline, Online } from 'react-detect-offline'
// @ts-ignore
import Refresh from 'react-feather/dist/icons/refresh-cw'
// @ts-ignore
import Download from 'react-feather/dist/icons/download'
import styles from '../styles'
import { State } from '../store/reducers/initialState'

import { wifiOn, wifiOff } from '../styles/img/icons'

type StateProps = Pick<State, 'day' | 'settings' | 'update'>

const FooterStyle = styles.footer

const updatedDisp = (settings: any) => {
  let result
  if (!settings.online) { result = null } else if (settings.updated < 1514764800) {
    result = (
      <div className="right">
        <Refresh size={24} />
        {' - '}
      </div>
    )
  } else {
    result = (
      <div className="right">
        <Refresh size={24} />
        {`${moment.duration(moment().diff(moment(settings.updated * 1000))).humanize()} ago`}
      </div>
    )
  }
  return result
}

// RAMADAN
const ramadanDisp = (day: any) => {
  const result = day.ramadanCountdown ? <div className="left">{`${day.ramadanCountdown} to Ramadan`}</div> : null
  return result
}

// JUMMUAH
const jummuahDisp = (settings: any) => {
  const jummuahTime = moment({
    hour: settings.jummuahtime[0],
    minute: settings.jummuahtime[1],
  }).day(5)

  const result = settings.jummuahtime[0] ? <div className="center">{`Jummuah ${jummuahTime.format('H:mm')}`}</div> : 0
  return result
}

// TARAWEEH
const taraweehDisp = (settings: any) => {
  const taraweehTime = moment({
    hour: settings.taraweehtime[0],
    minute: settings.taraweehtime[1],
  })

  const result =
    moment().format('iM') === '9' ? (
      <div className="left">{`${settings.labels.taraweeh} ${taraweehTime.format('H:mm')}`}</div>
    ) : null

  return result
}

// ONLINE
const onlineDisp = (settings: any) => {
  const result = settings.online ? (
    <div className="wifi">
      <Offline>{wifiOff}</Offline>
      <Online>{wifiOn}</Online>
    </div>
  ) : null
  return result
}

// REFRESHDIFF
const refreshDiffDisp = (settings: any, update: any) => {
  /* eslint-disable babel/no-unused-expressions */
  let diff
  update.downloaded !== '-' && moment.isMoment(update.downloaded)
    ? (diff = `${moment.duration(moment().diff(update.downloaded)).humanize()} ago (${settings.updateInterval})`)
    : (diff = '-')
  const result = settings.online ? (
    <div className="right">
      <Download size={24} />
      {diff}
    </div>
  ) : null

  return result
}

// MAPPING
const mapStateToProps = (state: StateProps) => ({
  day: state.day,
  settings: state.settings,
  update: state.update,
})

// COMPONENT
const ConnectedFooter = ({ day, settings, update }: StateProps) => (
  <FooterStyle>
    {onlineDisp(settings)}
    {jummuahDisp(settings)}
    {ramadanDisp(day)}
    {taraweehDisp(settings)}
    <div className="info">
      {refreshDiffDisp(settings, update)}
      {updatedDisp(settings)}
    </div>
  </FooterStyle>
)

// EXPORT
const Footer = connect(mapStateToProps)(ConnectedFooter)
export default Footer
