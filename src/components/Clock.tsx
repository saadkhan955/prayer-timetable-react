import * as React from 'react'

import { connect } from 'react-redux'

import styles from '../styles'

const ClockStyle = styles.clock
import { State } from '../store/reducers/initialState'
type StateProps = Pick<State, 'day'>

const mapStateToProps = ({ day }: StateProps) => ({ day })

const ConnectedClock = ({ day }: StateProps) => (
  <ClockStyle>
    <div className="body">{day.now.format('HH:mm:ss')}</div>
  </ClockStyle>
)

const Clock = connect(
  mapStateToProps
  // mapDispatchToProps
)(ConnectedClock)
export default Clock
