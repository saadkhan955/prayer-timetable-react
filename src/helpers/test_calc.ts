/* eslint-disable */
// @ts-ignore
import moment from 'moment-hijri'
// @ts-ignore
import momenttz from 'moment-timezone'

moment.locale('en') // to avoid indian numbers

const dayCalc = (offsetDay = 0, offSetHour = 0, hijrioffset = '0', city = 'Europe/Dublin') => {
  const now = moment()
    .add(offsetDay, 'day')
    .add(offSetHour, 'hour')
  const month = moment()
    .add(offsetDay, 'day')
    .add(offSetHour, 'hour')
    .month()
  const date = moment()
    .add(offsetDay, 'day')
    .add(offSetHour, 'hour')
    .date()

  const hijri = now.clone().add(hijrioffset, 'day')
  const start = moment().startOf('day')
  const end = moment().endOf('day')

  let dstAdjust
  const dstcheck = momenttz(moment(), city).isDST()

  if (!dstcheck && moment().format('M') === '10') {
    dstAdjust = -1
  } else if (dstcheck && moment().format('M') === '3') {
    dstAdjust = 1
  } else {
    dstAdjust = 0
  }

  return { now, month, date, start, end, hijri, dstAdjust }
}

const prayerCalc = (
  hourMinute: any,
  hourMinuteNext: any,
  index: any,
  now: any,
  when: any,
  jamaahmethods: any,
  jamaahoffsets: any,
  dstAdjust: any
) => {
  const [hour, minute] = hourMinute
  const [hourNext, minuteNext] = hourMinuteNext

  const time = moment({ hour, minute, month: now.month(), date: now.date() }).add(dstAdjust, 'hour')

  /* *********************** */
  /* JAMAAH CALC             */
  /* *********************** */
  const hourOffset = jamaahoffsets[index][0]
  const minuteOffset = jamaahoffsets[index][1]
  const jamaahmethod = jamaahmethods[index]
  let jtime
  let duration

  switch (jamaahmethod) {
    case 'afterthis':
      duration = moment.duration({ hours: hourOffset, minutes: minuteOffset })
      jtime = time.clone().add(duration)
      break
    case 'fixed':
      jtime = moment({ hour: hourOffset, minute: minuteOffset, month: now.month(), date: now.date() }).add(
        dstAdjust,
        'hour'
      )
      break
    case 'beforenext':
      duration = moment.duration({ hours: hourOffset, minutes: minuteOffset })
      jtime = moment({ hour: hourNext, minute: minuteNext, month: now.month(), date: now.date() })
        .subtract(duration)
        .add(dstAdjust, 'hour')
      break
    default:
      jtime = time
  }

  /* *********************** */
  /* NAMES                   */
  /* *********************** */
  const names = ['fajr', 'shurooq', 'dhuhr', 'asr', 'maghrib', 'isha']
  const name = names[index]
  const hasPassed = moment().isSameOrAfter(time)
  const isJamaahPending = moment().isBetween(time, jtime)

  const result = { time, isJamaahPending, jtime, index, hasPassed, name, when, dstAdjust }
  return result
}

const prayersCalc = (timetable: any, settings: any, showJamaah = true, city = 'Europe/Dublin') => {
  const { hijrioffset, jamaahmethods, jamaahoffsets } = settings
  const { now, month, date, start, hijri, dstAdjust } = dayCalc(0, 0, hijrioffset, city)
  const { now: nowYesterday, month: monthYesterday, date: dateYesterday, dstAdjust: dstAdjustYesterday } = dayCalc(
    -1,
    0,
    hijrioffset,
    city
  )
  const { now: nowTomorrow, month: monthTomorrow, date: dateTomorrow, dstAdjust: dstAdjustTomorrow } = dayCalc(
    1,
    0,
    hijrioffset,
    city
  )

  /* *********************** */
  /* SET PRAYERS             */
  /* *********************** */
  const prayersToday = timetable[month + 1][date].map((hourMinute: any, index: any) => {
    const hourMinuteNext = index < 5 ? timetable[month + 1][date][index + 1] : [24, 0]
    return prayerCalc(hourMinute, hourMinuteNext, index, now, 'today', jamaahmethods, jamaahoffsets, dstAdjust)
  })

  const prayersYesterday = timetable[monthYesterday + 1][dateYesterday].map((hourMinute: any, index: any) => {
    const hourMinuteNext = index < 5 ? timetable[month + 1][date][index + 1] : [24, 0]
    return prayerCalc(
      hourMinute,
      hourMinuteNext,
      index,
      nowYesterday,
      'yesterday',
      jamaahmethods,
      jamaahoffsets,
      dstAdjustYesterday
    )
  })
  const prayersTomorrow = timetable[monthTomorrow + 1][dateTomorrow].map((hourMinute: any, index: any) => {
    const hourMinuteNext = index < 5 ? timetable[month + 1][date][index + 1] : [24, 0]
    return prayerCalc(
      hourMinute,
      hourMinuteNext,
      index,
      nowTomorrow,
      'tomorrow',
      jamaahmethods,
      jamaahoffsets,
      dstAdjustTomorrow
    )
  })

  /* *********************** */
  /* PREVIOUS, CURRENT, NEXT */
  /* *********************** */
  let current
  let next
  let previous

  if (now.isBetween(start, prayersToday[0].time)) {
    previous = prayersYesterday[4]
    current = prayersYesterday[5]
    next = prayersToday[0]
  } else if (now.isBetween(prayersToday[0].time, prayersToday[1].time)) {
    previous = prayersYesterday[5]
    current = prayersToday[0]
    next = prayersToday[1]
  } else if (now.isBetween(prayersToday[1].time, prayersToday[2].time)) {
    previous = prayersToday[0]
    current = prayersToday[1]
    next = prayersToday[2]
  } else if (now.isBetween(prayersToday[2].time, prayersToday[3].time)) {
    previous = prayersToday[1]
    current = prayersToday[2]
    next = prayersToday[3]
  } else if (now.isBetween(prayersToday[3].time, prayersToday[4].time)) {
    previous = prayersToday[2]
    current = prayersToday[3]
    next = prayersToday[4]
  } else if (now.isBetween(prayersToday[4].time, prayersToday[5].time)) {
    previous = prayersToday[3]
    current = prayersToday[4]
    next = prayersToday[5]
  } else {
    previous = prayersToday[4]
    current = prayersToday[5]
    next = prayersTomorrow[0]
  }

  /* *********************** */
  /* COUNTDOWN/UP            */
  /* *********************** */
  const countUp = {
    name:
      current.isJamaahPending || !showJamaah
        ? current.name
        : `${current.name}${current.index !== 1 ? ' jamaah' : ''}`,
    time: current.isJamaahPending || !showJamaah ? current.time : current.jtime,
    duration:
      current.isJamaahPending || !showJamaah
        ? moment.duration(now.diff(current.time))
        : moment.duration(now.diff(current.jtime)),
  }
  const countDown = {
    name:
      current.isJamaahPending && showJamaah ? `${current.name}${current.index !== 0 ? ' jamaah' : ''}` : next.name,
    time: current.isJamaahPending && showJamaah ? current.jtime : next.time,
    duration:
      current.isJamaahPending && showJamaah
        ? moment.duration(current.jtime.diff(now)).add(1, 's')
        : moment.duration(next.time.diff(now)).add(1, 's'),
  }

  const totalDuration = countUp.duration.clone().add(countDown.duration)

  // const percentage = [totalDuration.hours(), totalDuration.minutes(), totalDuration.seconds() - 1]
  const percentageRaw = 10000 - (countDown.duration.asSeconds() / totalDuration.asSeconds()) * 10000
  const percentage = Math.floor(percentageRaw) / 100

  const isAfterIsha = moment().isSameOrAfter(prayersToday[5].jtime)
  const isJamaahPending = moment().isBetween(current.time, current.jtime)
  const focus = current.isJamaahPending ? current : next

  const result = {
    prayers: {
      today: prayersToday,
      yesterday: prayersYesterday,
      tomorrow: prayersTomorrow,
    },
    previous,
    current,
    next,
    countUp,
    countDown,
    now,
    hijri,
    percentage,
    isAfterIsha,
    isJamaahPending,
    focus,
  }
  return result
}

export { prayersCalc, dayCalc }
