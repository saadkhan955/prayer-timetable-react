/*eslint-disable*/

const wordCount = (str: string) => str.split(' ').filter(n => n !== '').length

const appendZero = (unit: number) => {
  if (unit < 10) { return `0${unit}` }
  return `${unit}`
}

export default { wordCount }
export { appendZero, wordCount }
